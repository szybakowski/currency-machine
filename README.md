# Currency Machine #
Reactive REST API with Spring WebFlux

### DOCKER-COMPOSE ###
Before build, run:
````
docker-compose up --build -d mongodb
````

### SWAGGER ###

````
http://localhost:8080/swagger-ui.html
````

### ENDPOINTS ###

Create user:
````
{POST [/user]}: createUser(CreateUserParams)
````
Get user info:
````
{GET [/user/{id}]}: getUser(String)
````
Exchange currency:
````
{PATCH [/exchange]}: exchangeBalance(ExchangeBalanceParams)
````