package com.bank.currency.machine.internal;

import java.math.BigDecimal;

public class User {
    private String id;
    private String name;
    private String lastName;
    private Currency currency;
    private BigDecimal balance;

    public User(String id, String name, String lastName, Currency currency, BigDecimal balance) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.currency = currency;
        this.balance = balance;
    }

    public static User init(String name, String lastName, BigDecimal balance) {
        User user = new User(null, name, lastName, Currency.PLN, balance);
        return user;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public Currency getCurrency() {
        return currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void exchangeBalance(Currency currency, BigDecimal balance) {
        this.currency = currency;
        this.balance = balance;
    }
}
