package com.bank.currency.machine.internal;

import com.bank.currency.machine.dtos.UserDTO;

public class UserMapper {

    public static UserDTO toUserDTO(User user) {
        return new UserDTO(
                user.getId(),
                user.getName(),
                user.getLastName(),
                user.getCurrency().toString(),
                user.getBalance()
        );
    }

}
