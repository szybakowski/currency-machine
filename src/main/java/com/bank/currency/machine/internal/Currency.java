package com.bank.currency.machine.internal;

public enum Currency {
    PLN("pln"),
    USD("usd");

    private String currency;

    Currency(String currency) {
        this.currency = currency;
    }

    public String getValue() {
        return currency;
    }

    public static Currency getEnum(String value) {
        for (Currency currency : values())
            if (currency.getValue().equalsIgnoreCase(value)) return currency;
        throw new IllegalArgumentException("Currency not allowed");
    }

}
