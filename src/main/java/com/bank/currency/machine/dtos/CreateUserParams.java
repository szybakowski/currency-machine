package com.bank.currency.machine.dtos;

import java.math.BigDecimal;

public final class CreateUserParams{
    private final BigDecimal startBalance;
    private final String name;
    private final String lastName;

    private CreateUserParams(BigDecimal startBalance, String name, String lastName) {
        this.startBalance = startBalance;
        this.name = name;
        this.lastName = lastName;
    }

    public BigDecimal getStartBalance() {
        return startBalance;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }
}
