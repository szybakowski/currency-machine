package com.bank.currency.machine.dtos;

import java.math.BigDecimal;

public class UserDTO {
    private String id;
    private String name;
    private String lastName;
    private String currency;
    private BigDecimal balance;

    public UserDTO(String id, String name, String lastName, String currency, BigDecimal balance) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.currency = currency;
        this.balance = balance;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCurrency() {
        return currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
