package com.bank.currency.machine.dtos;

import com.bank.currency.machine.internal.Currency;

public class ExchangeBalanceParams {
    private final String userId;
    private final Currency currency;

    public ExchangeBalanceParams(String userId, String currency) {
        this.userId = userId;
        this.currency = Currency.getEnum(currency);
    }

    public String getUserId() {
        return userId;
    }

    public Currency getCurrency() {
        return currency;
    }
}
