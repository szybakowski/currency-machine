package com.bank.currency.machine.service;

import com.bank.currency.machine.db.SpringDataUserRepository;
import com.bank.currency.machine.dtos.CreateUserParams;
import com.bank.currency.machine.dtos.UserDTO;
import com.bank.currency.machine.internal.User;
import com.bank.currency.machine.internal.UserMapper;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import javax.validation.ValidationException;
import java.math.BigDecimal;

@Service
public class UserService {

    private final SpringDataUserRepository userRepository;

    public UserService(SpringDataUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Mono<UserDTO> createUser(CreateUserParams createUserParams) {
        validateCreateUserParams(createUserParams);
        User user = User.init(createUserParams.getName(), createUserParams.getLastName(), createUserParams.getStartBalance());
        return userRepository.save(user)
                .map(UserMapper::toUserDTO);
    }

    public Mono<UserDTO> getUserDTO(String id) {
        return userRepository.getUser(id)
                .map(UserMapper::toUserDTO);
    }

    public void validateCreateUserParams(CreateUserParams createUserParams) {
        if (createUserParams.getName().isBlank() || createUserParams.getLastName().isBlank()) {
            throw new ValidationException("Name and LastName for new user is required");
        }
        if (createUserParams.getStartBalance().compareTo(BigDecimal.ZERO) <= 0) {
            throw new ValidationException("Balance for new user is required.");
        }
    }

}
