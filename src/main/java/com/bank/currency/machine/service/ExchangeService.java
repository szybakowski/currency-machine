package com.bank.currency.machine.service;

import com.bank.currency.machine.db.SpringDataUserRepository;
import com.bank.currency.machine.dtos.ExchangeBalanceParams;
import com.bank.currency.machine.external.NbpResponse;
import com.bank.currency.machine.internal.Currency;
import com.bank.currency.machine.internal.User;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.validation.ValidationException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;

@Service
public class ExchangeService {

    private final WebClient webClient;
    private final SpringDataUserRepository userRepository;

    public ExchangeService(WebClient webClient, SpringDataUserRepository userRepository) {
        this.webClient = webClient;
        this.userRepository = userRepository;
    }

    public Mono<User> exchange(ExchangeBalanceParams exchangeBalanceParams) {
        Mono<User> user = userRepository.getUser(exchangeBalanceParams.getUserId());
        if (exchangeBalanceParams.getCurrency() == Currency.PLN) {
            return user.map(this::exchangeBalanceForPLN)
                    .flatMap(Function.identity());
        }
        return user.map(u -> exchangeBalanceFromPLN(exchangeBalanceParams, u))
                .flatMap(Function.identity());
    }

    public Mono<User> exchangeBalanceFromPLN(ExchangeBalanceParams exchangeBalanceParams, User user) {
        validateExchangeBalanceParams(exchangeBalanceParams.getCurrency(), user.getCurrency());
        return getCurrencyRateFromNBP(exchangeBalanceParams.getCurrency().toString())
                .map(nbpResponse -> {
                    BigDecimal averageExchangeRate = nbpResponse.getRates().get(0).getMid();
                    BigDecimal balance = user.getBalance();
                    BigDecimal divide = balance.divide(averageExchangeRate, 2, RoundingMode.HALF_DOWN);
                    user.exchangeBalance(exchangeBalanceParams.getCurrency(), divide);
                    return userRepository.save(user);
                }).flatMap(Function.identity());
    }


    public Mono<User> exchangeBalanceForPLN(User user) {
        validateExchangeBalanceParams(Currency.PLN, user.getCurrency());
        return getCurrencyRateFromNBP(user.getCurrency().toString())
                .map(nbpResponse -> {
                    BigDecimal averageExchangeRate = nbpResponse.getRates().get(0).getMid();
                    BigDecimal balance = user.getBalance();
                    BigDecimal divide = balance.multiply(averageExchangeRate);
                    user.exchangeBalance(Currency.PLN, divide);
                    return userRepository.save(user);
                }).flatMap(Function.identity());
    }

    public void validateExchangeBalanceParams(Currency exchangeCurrency, Currency actualCurrency) {
        if (exchangeCurrency == actualCurrency) {
            throw new ValidationException("Cant exchange for same currency");
        }
    }

    public Mono<NbpResponse> getCurrencyRateFromNBP(String currency) {
        return webClient.get()
                .uri("/exchangerates/rates/a/" + currency + "/")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(NbpResponse.class)
                .retry(1);
    }

}
