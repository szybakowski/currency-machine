package com.bank.currency.machine.controller;

import com.bank.currency.machine.dtos.ExchangeBalanceParams;
import com.bank.currency.machine.service.ExchangeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.ValidationException;

@RestController
@RequestMapping("/exchange")
public class ExchangeController {

    private final ExchangeService exchangeService;

    public ExchangeController(ExchangeService exchangeService) {
        this.exchangeService = exchangeService;
    }

    @PatchMapping
    public Mono<ResponseEntity<Void>> exchangeBalance(@RequestBody ExchangeBalanceParams exchangeBalance) {
        return exchangeService.exchange(exchangeBalance)
                .then(Mono.just(ResponseEntity.noContent().build()));
    }

    @ResponseBody
    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<String> handleException(ValidationException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ResponseBody
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleException(IllegalArgumentException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
