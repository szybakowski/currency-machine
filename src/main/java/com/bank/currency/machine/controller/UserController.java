package com.bank.currency.machine.controller;

import com.bank.currency.machine.dtos.CreateUserParams;
import com.bank.currency.machine.dtos.UserDTO;
import com.bank.currency.machine.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.ValidationException;
import java.net.URI;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public Mono<ResponseEntity<UserDTO>> createUser(@RequestBody CreateUserParams createUserParams) {
        return userService.createUser(createUserParams)
                .map(userDTO -> ResponseEntity.created(URI.create("/user/" + userDTO.getId())).build());
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<UserDTO>> getUser(@PathVariable String id) {
        return userService.getUserDTO(id).map(ResponseEntity::ok);
    }

    @ResponseBody
    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<String> handleException(ValidationException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }


}

