package com.bank.currency.machine.db;

import com.bank.currency.machine.db.entity.UserEntity;
import com.bank.currency.machine.internal.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import javax.validation.ValidationException;

@Repository
public class SpringDataUserRepository {
    private final UserRepository userRepository;

    public SpringDataUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Mono<User> save(User user) {
        UserEntity userEntity = UserEntity.toEntity(user);
        return userRepository.save(userEntity)
                .map(UserEntity::toDomain);
    }

    public Mono<User> getUser(String id) {
        return userRepository.findById(id)
                .switchIfEmpty(Mono.error(new ValidationException("User not found - wrong id")))
                .map(UserEntity::toDomain);
    }
}

interface UserRepository extends ReactiveMongoRepository<UserEntity, String> {
}
