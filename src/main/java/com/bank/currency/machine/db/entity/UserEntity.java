package com.bank.currency.machine.db.entity;

import com.bank.currency.machine.internal.Currency;
import com.bank.currency.machine.internal.User;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Document(collection = "users")
public class UserEntity {
    @Id
    private String id;
    private String name;
    private String lastName;
    private Currency currency;
    private BigDecimal balance;

    public UserEntity() {
    }

    public static UserEntity toEntity(User user) {
        UserEntity userEntity = new UserEntity();
        userEntity.id = user.getId();
        userEntity.name = user.getName();
        userEntity.lastName = user.getLastName();
        userEntity.currency = user.getCurrency();
        userEntity.balance = user.getBalance();
        return userEntity;
    }

    public static User toDomain(UserEntity userEntity) {
        return new User(
                userEntity.getId(),
                userEntity.getName(),
                userEntity.getLastName(),
                userEntity.getCurrency(),
                userEntity.getBalance()
        );
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public Currency getCurrency() {
        return currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
