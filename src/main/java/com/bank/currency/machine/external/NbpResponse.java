package com.bank.currency.machine.external;

import java.util.List;

public class NbpResponse {

    public String code;
    public List<Rate> rates;

    public NbpResponse() {
    }

    public NbpResponse(String code, List<Rate> rates) {
        this.code = code;
        this.rates = rates;
    }

    public String getCode() {
        return code;
    }

    public List<Rate> getRates() {
        return rates;
    }
}

