package com.bank.currency.machine.external;

import java.math.BigDecimal;

public class Rate {
    public BigDecimal mid;

    public Rate() {
    }

    public Rate(BigDecimal mid) {
        this.mid = mid;
    }

    public BigDecimal getMid() {
        return mid;
    }
}